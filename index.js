// console.log("Hello World");

// What are conditional statements?
// Conditional Statements allow us to control the flow of our program and it allow us to run a statement/instruction based on the condition.

// [SECTION] if, else if, else statement

let numA = 0;

// if statement
// Executes a statement if a specified condition is true.
/*
	Syntax:

	if(condition){
		code block (statement);
	}
*/

if(numA < 0){
	console.log("Hello");
}

// The result of the expression in the if statement must result to "true", else it will not run the statement inside.

// this is used for checking the value
console.log(numA < 0);

let city = "New York";

// always make it a strict equality operator
if (city === "New York"){
	console.log("Welcome to New York City!");
}

// else if clause
/*
	- Executes a statement if previous condition are false and if the specified condition is true.
	- The "else if" clause is optional and can be added to capture additional conditions to change the flow of a program.

*/

let numH = 0;

 if (numH < 0){
 	console.log("Hello");
 }
 else if (numH > 0){
 	console.log("World");
 }

city = "Manila";
if (city === "New York"){
	console.log("Welcome to New York City!");
}

else if (city === "Tokyo"){
	console.log("Welcome to Tokyo, Japan");
}


// else Statement
/*
	- Executes a statement if all other condition are false.
	- The 'else' statement is optional and can be added to capture any other result to change the flow of program

*/

if (numH < 0){
 	console.log("Hello");
 }

 else if (numH > 0){
 	console.log("World");
 }

 else{
 	console.log("again");
 }

 if (city === "New York"){
	console.log("Welcome to New York City!");
}

else if (city === "Tokyo"){
	console.log("Welcome to Tokyo, Japan");
}

else{
	console.log("City is not included in the list.")
}

// if, else if and else statement inside a function

/*
	Scenario: We want to determine intensity of a typhoon based on its wind speed.
		Not a Typoon - Wind speed is less than 30. 
		Tropical Depression - Wind speed is less than or equal to 61.
		Tropical Storm - Wind speed is 62 to 88.
		Severe Tropical Storm - Wind speed is between 89 to 117.
		Typoon - Wind speed is greater than or equal to 118.
*/

	let message = "No message";
	console.log(message);

	function determineTyphoonIntensity(windSpeed){

		if(windSpeed < 30){
			return "Not a typhoon yet"
		}

		else if(windSpeed <= 61){
			return "Tropical Depression detected"
		}

		// AND STATEMENT
		else if (windSpeed >=62 && windSpeed <=88){
			return "Tropical storm detected"
		}

		//OR STATEMENT
		else if (windSpeed >=89 && windSpeed <=117){
			return "Severe Tropical Storm detected"
		}

		else{
			return "Typhoon detected"
		}

	}

	message = determineTyphoonIntensity(110);
	console.log(message);

	// console.warn() a good way to print warnings in our console that could help us developers act on a certain output within our code.
	if(message === "Severe Tropical Storm detected"){
		console.warn(message);
	}

// [SECTION] Truthy and Falsy
/*		
	- In Javascript a "truthy" value is a value that is considered true when encountered in a boolean context
	- Falsy values/exception for truthy:
		1. false
		2. 0
		3. ""
		4. null
		5. undefined
		6. NaN (Not a Number)
*/
	
	let isMarried = true;

	// Truthy examples:
	if (true){
		console.log("Truthy");
	}

	if(1){
		console.log("Truthy");
	}

	if([]){
		console.log("Truthy");
	}

	// Falsy examples:
	if(false){
		console.log("Falsy");
	}

	if(0){
		console.log("Falsy");
	}

	if(undefined){
		console.log("Falsy");
	}
	
	 if(isMarried){
	 	console.log("Truthy");
	 }

// [SECTION] Conditional (Ternary) Operator
/*
	- The conditional (ternary) operator takes in three operands
	1. condition
	2. expression to execute if the value is truthy
	3. expression to execute if the value is falsy 
	
	- alternative for an "if else" statement
	- ternary operators have an "implicit return" statement meaning that without the "return" keyword, the resulting expressions can be stored in a variable.
	- Commonly used for single statement execution where the result consists of only 1 line of code.
*/

/*

	Syntax:
		(expression) ? ifTrue : ifFalse;

*/

let ternaryResult = (18 < 18) ? true : false;
console.log("Result of ternary operator: "+ ternaryResult);

// Multiple statement execution using ternary operator

let name;

function isOfLegalAge(){
	name = " John";
	return "You are of legal age limit";
}

function isUnderAge(){
	name = " Jane";
	return "Your are under the age limit";
}

// parseInt converts the input received into a number data type
	// if an input value is not a number, it will return NaN (Not a Number)
let age = parseInt(prompt("What is your age?"));
console.log(age);

let legalAge = (age >= 18) ? isOfLegalAge() : isUnderAge();
console.log("Result of ternary operator in functions: "+legalAge+ ',' + name);

/*
	We can also create Nested if statements based on specific:
	if(expression){
		if(expression){
			//code block //una muna tong sa loob before ung sa labas
		}
		else{
			//code block //una muna tong sa loob before ung sa 
		}
	}


*/

// [SECTION] Switch Statement
/*
	- The switch statement evaluates an expression and matches the expression's value to a case clause.
	- Can be used as an alternative to an "if, else if, and else" statements where the data to be used in the condition is of an expected output.
	- The ".toLowerCase()" function/method will change the input received from the prompt to lowercase letters ensuring a match with switch case condition.
	- The break statement is used to terminate the current loop once a match has found.
	- Switch cases are considered "loops" meaning it will compare the "expression" with each of the case "value" until a match found.



	Syntax:
		switch(expression){
			case value:
				statement/code block;
				break;
			default:
				statement/code block;
				break;
		}

*/

let day = prompt("What day of the week is it today?").toLowerCase();
console.log(day);

switch(day){
	case "monday":
		console.log("The color of the day is red.");
		break;
	case "tuesday":
		console.log("The color of the day is orange.");
		break;
	case "wednesday":
		console.log("The color of the day is yellow.");
		break;
	case "thursday":
		console.log("The color of the day is green.");
		break;
	case "friday":
		console.log("The color of the day is blue.");
		break;
	case "saturday":
		console.log("The color of the day is indigo.");
		break;
	case "sunday":
		console.log("The color of the day is violet.");
		break;
	default:
		console.log("Please input a valid day");
		break;
}

// [SECTION] Try-Catch-Finally Statement
/*
	-"try catch" statements are commonly used for error handling.
	- it is also useful for debugging code because of the "error" object that can be caught using the try catch statement.

	Syntax:
		try{
			// code block that we try to execute
		}
		// error/err are commonly used variable names for storing errors
		catch(error/err){
			//catch the and display the error message
		}
		finally{
			//continue execution of code regardless of the success and failure of the code execution.
		}
*/

function showIntensityAlert(windSpeed){
	try{
		// error: alerat is not defined
		alert(determineTyphoonIntensity(windSpeed));
	}
	catch (error){
		//"error.message" is used to access the information relating to an error object.
		console.warn(error.message);
	}
	finally{
		alert("Intensity updates will show new alert.");
	}
	
}

showIntensityAlert(56);
console.log("Hello world again!");